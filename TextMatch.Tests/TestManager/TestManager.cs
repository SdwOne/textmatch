﻿//	Defines the unit testing class for the Logic.Manager.
//	-----------------------------------------------------
//	I've subsumed most tests into one testing method.
//	Normally, I would test a multiple of different conditions,
//	using multiple testing methods (and 'Data Driven Testing' techniques).

using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ArgumentNullException = System.ArgumentNullException;

namespace TextMatch.Tests
{
	using Model = Models.Model;
	using Manager = Logic.Manager;

	[TestClass]
	public class TestManager
	{
		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void GenerateOutput_NullModel_Exception()
		{
			Manager.GenerateOutput(null);
		}

		[TestMethod]
		public void GenerateOutput_TestData_Pass()
		{
			// Arrange
			var data = new Dictionary<string, string> // Test data
			{
				{ "Polly",  "1,26,51"},
				{ "ll",     "3,28,53,78,82"},
				{ "X",      string.Empty},
				{ "Polx",   string.Empty}, // Further tests can be added here without changing the rest of the code.
			};
			var model = new Model // Test model
			{
				Text = "Polly put the kettle on, polly put the kettle on, polly put the kettle on we’ll all have tea"
			};
			// Test loop
			foreach (var datum in data)
			{
				model.SubText = datum.Key;                  // Arrange
				Manager.GenerateOutput(model);              // Act
				Assert.AreEqual(datum.Value, model.Output); // Assert
			}
		}
	}
}