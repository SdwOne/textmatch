﻿//	Defines the core logic of this application.
//	-------------------------------------------
//	This class constitutes the 'business logic layer' and would normally reside in another class library.
//	It's role is to simply process validated models from the controller before they are passed to the view.

using System.Collections.Generic;
using ArgumentNullException = System.ArgumentNullException;

namespace TextMatch.Logic
{
	using Model = Models.Model;

	public static class Manager
	{
		// Core logic encapsulated here so that it's easier to reuse in future projects.
		private static string FindMatchPositions(string input, string find)
		{
			// Check if arguments are 'valid' otherwise return 'string.Empty'.
			if (string.IsNullOrWhiteSpace(input) || string.IsNullOrWhiteSpace(find))
				return string.Empty;

			var findIndex = 0;                       // Character index for the 'find' string.
			var matchPositions = new List<string>(); // Accumulates found positions.

			// Loop all input characters.
			for (var i = 0; i < input.Length; ++i)
			{
				// Check if a character is found.
				if (char.ToLower(input[i]) == char.ToLower(find[findIndex]))
				{
					// Increments 'findIndex' and check if we've reached the end of the 'find' string.
					if (++findIndex == find.Length)
					{
						matchPositions.Add((i - findIndex + 2).ToString()); // If so, add the position based on loop index 'i' and 'findIndex'.
						findIndex = 0;                                      // Reset 'findIndex'.
					}
				}
				else if (findIndex > 0) // If no character is found, check if 'findIndex' was previously set.
					findIndex = 0;      // If so, reset 'findIndex'.
			}
			// Return a comma deliminated string of the positions.
			// If 'matchPositions' is empty, 'string.Join' returns 'string.Empty'.
			return string.Join(",", matchPositions);
		}

		public static void GenerateOutput(Model model)
		{
			// Always good to check the validity of arguments for exposed methods!
			if (model == null)
				throw new ArgumentNullException(nameof(model));

			// Set the model 'Output' property to the results returned from 'FindMatchPositions'.
			model.Output = FindMatchPositions(model.Text, model.SubText);
		}
	}
}