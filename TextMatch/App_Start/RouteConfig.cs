﻿//	Defines the HTTP routing configuration of this application.
//	-----------------------------------------------------------

using System.Web.Mvc;
using RouteCollection = System.Web.Routing.RouteCollection;

namespace TextMatch
{
	internal static class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			// Enable MVC5+ attribute routing
			routes.MapMvcAttributeRoutes();
		}
	}
}