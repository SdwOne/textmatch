﻿//	Defines the root controller of this application.
//	------------------------------------------------
//	The use of 'TempData' violates the RESTful paradigm.
//	I was tempted to use jQuery AJAX but decided to go along with a posted form.

using System.Web.Mvc;

namespace TextMatch.Controllers
{
	using Model = Models.Model;
	using Manager = Logic.Manager;

	public sealed class HomeController : Controller
	{
		// Defines the name of the controller (without the 'Controller' postfix)
		public static readonly string Name = nameof(HomeController).Replace(nameof(Controller), null);

		[Route]
		[HttpGet]
		public ViewResult Index()
		{
			// Set 'model' to the one stored in 'TempData'.
			var model = (Model)TempData[nameof(Model)];

			// Return a view of 'model' (if it exists) or a view of a new model.
			return View((model != null) ? model : new Model());
		}

		[Route]
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Index(Model model)
		{
			// Check model validity.
			if (!ModelState.IsValid)
				return View(model);

			Manager.GenerateOutput(model);          // Generate output on the model.
			TempData[nameof(Model)] = model;        // Store model in 'TempData' (violates RESTful!).
			return RedirectToAction(nameof(Index)); // Redirect to the root page.
		}
	}
}