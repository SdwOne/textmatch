﻿//	Defines the model of this application.
//	--------------------------------------
//	Normally, I would use a separate class library to encapsulate models.
//	I would also use separate libraries for entities (ORM models) and view (binding) models.

namespace TextMatch.Models
{
	public sealed class Model
	{
		public string Text { get; set; }
		public string SubText { get; set; }
		public string Output { get; set; }
	}
}