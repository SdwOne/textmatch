**Royal Pharmaceutical Society Technical Test.**

This is a C# / MVC5 web-application which targets the .NET Framework 4.6.1 platform.

A unit testing project is also included in the solution.