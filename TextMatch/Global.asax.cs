﻿//	Defines the HTTP application startup class.
//	-------------------------------------------

using RouteTable = System.Web.Routing.RouteTable;
using HttpApplication = System.Web.HttpApplication;

namespace TextMatch
{
	public class Application : HttpApplication
	{
		protected void Application_Start()
		{
			RouteConfig.RegisterRoutes(RouteTable.Routes);
		}
	}
}